rotate _ [] = []
rotate n xs = zipWith const (drop n (cycle xs)) xs

string xs = map (\x -> rotate x xs) [1..length(xs)]

main = do
    n <- readLn :: IO Int
    input <- getContents
    --let str = map read (lines input)
    putStrLn $ show input 
    --putStrLn . unlines $ (map show . string) str

--------------------------------------------             IO in HAESKELL  ----------------------------------------------           

--	1:> Scanning 2 integer from STDIN, calling a function andthen print output to STDOUT....
		solveMeFirst a b = a + b
		main = do
    			val1 <- readLn
    			val2 <- readLn
    			let sum = solveMeFirst val1 val2
    			print sum
--_____________________________________________________________________________________________________________________
--	2:> Tacking string as a input and print it to STDOUT....
		putStr "hello" is print to STDOUT
		putStrLn "hello" is same as putStr but add new line character. 
--_____________________________________________________________________________________________________________________	
--	3:> Print hello world n time Loop....
		hello_worlds 0 = putStr ""
		hello_worlds x = do
			putStrLn "Hello World"
                    	hello_worlds (x-1)

		main = do
   			n <- readLn :: IO Int
			hello_worlds n		
--_____________________________________________________________________________________________________________________
--	4:> take input and repeat element n time....
		f :: Int -> [Int] -> [Int]
		fn n x = take n (repeat x)
		f _ [] = []
		f n (x:arr) = (fn n x) ++ f n arr	
		main :: IO ()
		main = getContents >>=
       			mapM_ print. (\(n:arr) -> f n arr). map read. words 
--_____________________________________________________________________________________________________________________	
--	5:> take a input x and compare it with other if it is less then print is....
		f :: Int -> [Int] -> [Int]
		f n arr = [x | x<-arr,x<n]
		main = do 
    			n <- readLn :: IO Int 
    			inputdata <- getContents 
    			let 
        			numbers = map read (lines inputdata) :: [Int] 
    			putStrLn . unlines $ (map show . f n) numbers
--_____________________________________________________________________________________________________________________	
--	6:> take a input of list and print values which is at even place....
		f :: [Int] -> [Int]
		f x = map ((!!) x) [1,3..(length x - 1)]
		main = do
   			inputdata <- getContents
   			mapM_ (putStrLn. show). f. map read. lines $ inputdata
--_____________________________________________________________________________________________________________________	
--	7:> Reverse a list.....
		rev [] = []
		rev (x:lis) = (rev lis) ++ [x] 
--_____________________________________________________________________________________________________________________
--	8:> Sum of odd elements.....
		f arr = sum [x | x<-arr,odd x]
		main = do
   			inputdata <- getContents
   			putStrLn $ show $ f $ map (read :: String -> Int) $ lines inputdata
--_____________________________________________________________________________________________________________________

----------------------------------------------      Bit manupulation     ------------------------------------------------------
--To clear bit for the last use
import Data.Bits
clearBit n (k-1)

---------------------------------------------------- Type In Haskell ------------------------------------------------------
--
-- Sure! mapM (with or without the _) is similar to fmap/map, except it works with functions that return monadic values.
--  
--To explain more, the signature of mapM is (Traversable t, Monad m) => (a -> m b) -> t a -> m (t b). The relevant info about Traversable is that all Traversable types are also Foldable and Functors.
--(removed)
--So mapM applies a unary/1-argument function returning a monadic value to every element or value inside another structure, and flips around the outer types.
--mapM_ is for functions with a return of m (), i.e. functions that do some sort of side effect, like printing.
--Which means that we'd want mapM_ if we want to print a bunch of values inside a list, tree, Maybe, etc. etc. etc.
--1 moment, trying to figure out how to monospace an extended code snippet
--
--Prelude> mapM_ print [1, 2, 3]
--1
--2
--3
--
--Now because we're working with a list of lists ([[a]]), we want a function that will work with that. The type of print is a -> IO (). So we need to change the second argument to something with type (Traversable t) => t a -> IO (). Think you can do that?
--_____________________________________________________________________________________________________________________
